import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
// import Database from '@ioc:Adonis/Lucid/Database'

// models
import Venue from 'App/Models/Venue'

export default class VenuesController {
    public async index ({request, response}: HttpContextContract) {
        if (request.qs().name) {
            let name = request.qs().name
            // let venuesFiltered = await Database.from('venues').select('id', 'name', 'address', 'phone').where('name', name)
            let venuesFiltered = await Venue.findBy('name', name)
            return response.status(200).json({message: 'Success get venue', data: venuesFiltered})
            
        }
        else {
            // let venues = await Database.from('venues').select('id', 'name', 'address', 'phone')
            let venues = await Venue.all()
            return response.status(200).json({message: 'Success get venue', data: venues})
        }
    }

    public async store ({request, response}: HttpContextContract) {
        await request.validate(CreateVenueValidator)
        // let newVenuesId = await Database.table('venues').returning('id').insert({
        //     name: request.input('name'),
        //     address: request.input('address'),
        //     phone: request.input('phone')
        // })
        let newVenue = new Venue()
        newVenue.name = request.input('name')
        newVenue.address = request.input('address')
        newVenue.phone = request.input('phone')

        await newVenue.save()
        response.status(200).json(
            { message: 'created', newId: newVenue.id, venue: newVenue }
        )
    }

    public async show ({params, response}: HttpContextContract) {
        // let venues = await Database.from('venues').where('id', params.id).select('id', 'name', 'address', 'phone').firstOrFail()
        let venues = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        return response.ok({message: 'Success get venue with id', data: venues})
    }

    public async update ({request, response, params}: HttpContextContract) {
        let id = params.id
        // await Database.from('venues').where('id', id).update({
        //     name: request.input('name'),
        //     address: request.input('address'),
        //     phone: request.input('phone')
        // })
        let venues = await Venue.findOrFail(id)
        venues.name = request.input('name')
        venues.address = request.input('address')
        venues.phone = request.input('phone')
        venues.save()
        return response.ok({message: 'Success updated venue'})
    }

    public async destroy ({params, response}: HttpContextContract) {
        // await Database.from('venues').where('id', params.id).delete()
        let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        return response.ok({message: 'Success deleted venue'})
    }
    
    public async bookings ({request, response}: HttpContextContract) {
        const payload = await request.validate(CreateBookingValidator)
        response.status(200).json(
            { booking: payload }
        )
    }
}

