import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
  public async index ({request, response, params}: HttpContextContract) {
    if (request.qs().name) {
      let name = request.qs().name
      let fieldsFiltered = await Database.from('fields').select('id', 'name', 'type', 'venue_id').where({'name': name, 'venue_id': params.venue_id}).firstOrFail()
      // let fieldsFiltered = await Field.findBy('name', name)
      return response.status(200).json({message: 'Success get field', data: fieldsFiltered})
    }
    else {
      let fields = await Database.from('fields').select('id', 'name', 'type', 'venue_id').where({'id': params.id, 'venue_id': params.venue_id}).firstOrFail()
      // let fields = await Field.all()
      return response.status(200).json({message: 'Success get field', data: fields})
    }
  }

  // public async create ({}: HttpContextContract) {
  // }

  public async store ({request, response, params}: HttpContextContract) {
    await request.validate(CreateFieldValidator)
    // let newFieldsId = await Database.table('fields').returning('id').insert({
    //     name: request.input('name'),
    //     type: request.input('type'),
    //     venue_id: parseInt(params.venue_id)
    // })
    const venue = await Venue.findByOrFail('id', params.venue_id)
    const newField = new Field()
    newField.name = request.input('name')
    newField.type = request.input('type')

    await newField.related('venue').associate(venue)
    return response.created(
        { status: 'success', data: newField }
    )
  }

  public async show ({params, response}: HttpContextContract) {
    const field = await Field.query().where('id', params.id).preload('booking', (bookingQuery) => {
      bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
    }).firstOrFail()
    // let fields = await Field.find(params.id)
    return response.ok({status: 'Success get field with id', data: field})
  }

  // public async edit ({}: HttpContextContract) {
  // }

  public async update ({request, response, params}: HttpContextContract) {
    await Database.from('fields').where({'id': params.id, 'venue_id': params.venue_id}).update({
        name: request.input('name'),
        type: request.input('type'),
        venue_id: parseInt(params.venue_id)
    })
    // let field = await Field.findOrFail(params.id)
    // field.name = request.input('name'),
    // field.type = request.input('type'),
    // field.venue_id = parseInt(params.venue_id)
    // field.save()
    return response.ok({message: 'Success updated field'})
  }

  public async destroy ({params, response}: HttpContextContract) {
    // await Database.from('fields').where('id', params.id).delete()
    let field = await Field.findOrFail(params.id)
    await field.delete()
    return response.ok({message: 'Success deleted fields'})
  }
}
